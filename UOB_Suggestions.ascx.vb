﻿Imports Microsoft.Office.Server.Search.Administration
Imports Microsoft.Office.Server
Imports Microsoft.SharePoint
Imports Microsoft.SharePoint.Administration
Imports Microsoft.Office.Server.Search.Administration.Query
Imports System.Collections.Generic

Partial Class UOB_Suggestions
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Add_OR_RemoveSuggestions()
    End Sub

    Public Sub Add_OR_RemoveSuggestions()
        '  Try

        '  Dim suggestion As String = Nothing
        If Request.QueryString.Count > 0 Then

         SPSecurity.RunWithElevatedPrivileges(Sub()

                                                  Dim result As SearchServiceApplication = Nothing
                                                  '' getting proxy
                                                  Dim searchProxy As SearchServiceApplicationProxy = DirectCast(SearchServiceApplicationProxy.GetProxy(SPServiceContext.GetContext(SPContext.Current.Site)), SearchServiceApplicationProxy)
                                                  ''  getting serach service application
                                                  If searchProxy IsNot Nothing Then
                                                      Dim ssaId As Guid = searchProxy.GetSearchServiceApplicationInfo().SearchServiceApplicationId
                                                      result = TryCast(SearchService.Service.SearchApplications(ssaId), SearchServiceApplication)
                                                  End If
                                                  If Not IsNothing(result) Then
                                                      Dim serviceApplicationOwner As New SearchObjectOwner(SearchObjectLevel.SPSite, SPContext.Current.Site.RootWeb)

                                                      'Dim serviceApplicationOwner As New SearchObjectOwner(SearchObjectLevel.Ssa)
                                                      Dim serviceApplicationResultSource As SourceRecord = searchProxy.GetResultSourceByName("Local SharePoint Results", serviceApplicationOwner)
                                                      'Response.Write(result.Name)
                                                      'Response.Write (serviceApplicationResultSource.Name )

                                                      Dim serviceApplicationResultSourceId As Guid = serviceApplicationResultSource.Id
                                                      Dim ranking As New Ranking(result, serviceApplicationOwner)

                                                      'replace EN-US with your language of choice
                                                      'Dim suggestions As LanguageResourcePhraseList = ranking.LanguageResources("EN-US").QuerySuggestionsAlwaysSuggestList
                                                      Dim suggestions As LanguageResourcePhraseList = Nothing

                                                      If Not IsNothing(Request.QueryString("removequery")) Then
                                                          suggestions = ranking.LanguageResources("EN-US").QuerySuggestionsBlockList

                                                          Try
                                                              suggestions.AddPhrase(Nothing, Request.QueryString("removequery"), String.Empty)
                                                              lblsuggestionresult.Text = "Sucessfully Added in Query Suggestions BlockList"

                                                              '  Response.Write("sucessfully added in query suggestion ")
                                                          Catch ex As Exception
                                                              lblsuggestionresult.Text = "Already added in Query Suggestions BlockList"

                                                              '  Response.Write("already added in query suggestion ")
                                                          End Try

                                                          'Dim boolremovesuggestion As Boolean = False
                                                          'Dim listofsugguestion As New List(Of String)
                                                          'For Each ss As LanguageResourcePhrase In suggestions

                                                          '    If Request.QueryString("removequery").ToLower.Trim = ss.Key.Phrase.ToLower.Trim Then
                                                          '        boolremovesuggestion = True
                                                          '        ' Response.Write("BOOLFIND" + "<BR/>")
                                                          '    Else
                                                          '        listofsugguestion.Add(ss.Key.Phrase)
                                                          '    End If

                                                          '   Response.Write(ss.Key.Phrase + "<br/>")
                                                          'Next

                                                          'If boolremovesuggestion = True Then

                                                          '    suggestions.RemoveAllPhrases()
                                                        '  lblsuggestionresult.Text = "Sucessfully Removed form query suggestions"
                                                          '   Response.Write("sucessfully Removed form query suggestion")

                                                          '    For Each suggestionstr As String In listofsugguestion
                                                          '        suggestions.AddPhrase(Nothing, suggestionstr, String.Empty)
                                                          '    Next
                                                          'End If

                                                      End If
                                                      'suggestions.RemoveAllPhrases()
                                                      '  suggestions.AddPhrase(serviceApplicationResultSourceId, suggestion, String.Empty)
                                                      If Not IsNothing(Request.QueryString("addquery")) Then
                                                          suggestions = ranking.LanguageResources("EN-US").QuerySuggestionsAlwaysSuggestList

                                                          Try
                                                              suggestions.AddPhrase(Nothing, Request.QueryString("addquery"), String.Empty)
                                                              lblsuggestionresult.Text = "Sucessfully Added in Query Suggestions "

                                                              '  Response.Write("sucessfully added in query suggestion ")
                                                          Catch ex As Exception
                                                              lblsuggestionresult.Text = "Already added in Query Suggestions "

                                                              '  Response.Write("already added in query suggestion ")
                                                          End Try
                                                      End If

                                                      '    'get the job that processes suggestions and run it
                                                      '    '  Dim job As SPJobDefinition = SPFarm.Local.GetObject(entry.JobDefinitionId)
                                                      '    ' Dim job As SPJobDefinition = SPFarm.Local.Services.OfType(Of SearchService)().SelectMany(Function(x) x.JobDefinitions).Where(Function(x) x.Name = "Prepare query suggestions").[Single]()

                                                      '''''' NO NEED TO RUN TIMMER JOB  bcz after 2 min it will auto update''''''''''''

                                                      '    Dim myjob As SPJobDefinition = Nothing

                                                      '    For Each job As SPJobDefinition In SPContext.Current.Site.WebApplication.JobDefinitions
                                                      '        If job.Name.Equals("Prepare query suggestions", StringComparison.OrdinalIgnoreCase) Then
                                                      '            myjob = job
                                                      '            Exit For
                                                      '        End If
                                                      '    Next
                                                      '    If Not IsNothing(myjob) Then
                                                      '        myjob.RunNow()
                                                      '    End If
                                                  End If

 End Sub)
        End If
        'Catch ex As Exception
        '    Response.Write(ex.Message + ex.StackTrace)
        'End Try

    End Sub

End Class